import 'package:flutter/material.dart';

const Color color1 = Color(0xFF4e565f);
const Color color2 = Color(0xFF6d8088);
const Color color3 = Color(0xFF9fdbe6);
const Color color4 = Color(0xFF50838d);
const Color color5 = Color(0xFF111e20);