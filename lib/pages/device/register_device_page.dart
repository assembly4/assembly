import 'package:assembly/pages/device/devices_controller.dart';
import 'package:assembly/themes/color_palette.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_notifier.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

class RegisterDevicePage extends GetView<DevicesController> {
  const RegisterDevicePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("New device discovered"),
        backgroundColor: color4,
      ),
      body: ListView(
        children: [
          const ListTile(
            leading: Text(
              "New",
              style: TextStyle(color: Colors.white),
            ),
            tileColor: color2,
          ),
          Obx(
            () => ListTile(
              onTap: () => controller.select(-1),
              selectedTileColor: color3,
              selectedColor: Colors.black87,
              selected: controller.index.value == -1 ? true : false,
              title: Text(controller.newDevice.value.name),
              subtitle: Text(controller.newDevice.value.uuid),
            ),
          ),
          const ListTile(
            leading: Text(
              "Registered",
              style: TextStyle(color: Colors.white),
            ),
            tileColor: color2,
          ),
          controller.obx(
            (state) => Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                for (int i = 0; i < state!.devices.length; i++)
                  ListTile(
                    onTap: () => controller.select(i),
                    selectedTileColor: color3,
                    selectedColor: Colors.black87,
                    selected: controller.index.value == i ? true : false,
                    title: Text(state.devices[i].name),
                    subtitle: Text(state.devices[i].uuid),
                  ),
              ],
            ),
          ),

        ],
      ),
      bottomNavigationBar: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Obx(
                  () => ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: color4,
                  fixedSize: const Size.fromHeight(60),
                  shape: const BeveledRectangleBorder(),
                ),
                onPressed: controller.index.value != -2
                    ? controller.chooseDevice
                    : null,
                child: const Text("Confirm"),
              ),
            ),
          ),
          const Padding(padding: EdgeInsets.all(1)),
          Expanded(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: color4,
                fixedSize: const Size.fromHeight(60),
                shape: const BeveledRectangleBorder(),
              ),
              onPressed: controller.skip,
              child: const Text("Skip"),
            ),
          ),
        ],
      ),
    );
  }
}
