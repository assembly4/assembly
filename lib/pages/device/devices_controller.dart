import 'dart:async';
import 'package:assembly/components/services/models.dart';
import 'package:assembly/components/services/services.dart';
import 'package:get/get.dart';
import 'package:uuid/uuid.dart';
import 'package:hive_flutter/hive_flutter.dart';

class DevicesController extends GetxController with StateMixin<DevicesModel> {
  final newDevice = DeviceModel(
    name: Hive.box('box').get("hardwareName", defaultValue: 'unknown'),
    owner: Hive.box('claimsBox').get("Email"),
    uuid: const Uuid().v4(),
  ).obs;
  final index = (-2).obs;

  final deviceBox = Hive.box('deviceBox');
  final claimsBox = Hive.box('claimsBox');

  DeviceModel selectedDevice = DeviceModel(
    name: "",
    owner: Hive.box('claimsBox').get("Email"),
    uuid: "",
  );

  late Timer t1;

  @override
  void onInit() {
    querySelfDevices();
    super.onInit();
    t1 = Timer.periodic(const Duration(seconds: 10), (_) => querySelfDevices());
  }

  @override
  void onClose() {
    t1.cancel();
    super.onClose();
  }

  void select(int i) {
    index.value = i;
    if (i == -1) {
      selectedDevice.uuid = newDevice.value.uuid;
      selectedDevice.name = newDevice.value.name;
    } else {
      selectedDevice.uuid = state!.devices[i].uuid;
      selectedDevice.name = state!.devices[i].name;
    }
    update();
  }

  void chooseDevice() {
    // try to find device
    DeviceModel? existingItem = state!.devices.firstWhereOrNull(
      (itemToCheck) => itemToCheck == selectedDevice,
    );

    // if device doesn't exist in DB - put
    if (existingItem == null) {
      try {
        s.geneva.putDevice(selectedDevice.uuid, selectedDevice.name);
      } catch (err) {
        s.logger.e("DeviceController.chooseDevice", err);
        return;
      }
    }

    // save device uuid locally and proceed
    deviceBox.put("device_name", selectedDevice.name);
    deviceBox.put("device_uuid", selectedDevice.uuid);
    Get.offAndToNamed('/');
  }

  void skip() => Get.offAndToNamed('/');

  Future<void> querySelfDevices() async {
    try {
      String user = await claimsBox.get("Email");
      DevicesModel data = await s.geneva.getDevices(user);
      data.devices.isEmpty
          ? change(data, status: RxStatus.empty())
          : change(data, status: RxStatus.success());
    } catch (err) {
      change(null, status: RxStatus.error("error"));
    }
  }
}
