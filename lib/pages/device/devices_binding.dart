import 'package:assembly/pages/device/devices_controller.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:get/get_instance/src/extension_instance.dart';

class DevicesBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(DevicesController());
  }
}
