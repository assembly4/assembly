import 'package:assembly/components/services/services.dart';
import 'package:assembly/pages/home/worker_controller.dart';
import 'package:assembly/themes/color_palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/state_manager.dart';
import 'package:hive_flutter/hive_flutter.dart';

class SettingsView extends GetView<LocStreamWorkerController> {
  SettingsView({Key? key}) : super(key: key);

  final claimsBox = Hive.box('claimsBox');
  final deviceBox = Hive.box('deviceBox');

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          color: color4,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 8.0,
              vertical: 25,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const CircleAvatar(
                  radius: 45,
                  backgroundColor: color3,
                  child: Icon(
                    Icons.person,
                    color: color4,
                    size: 35,
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                ValueListenableBuilder<Box>(
                  valueListenable: claimsBox.listenable(keys: ['Email']),
                  builder: (context, box, widget) {
                    return Text(
                      box.get('Email', defaultValue: "unknown"),
                      style: const TextStyle(
                        color: Colors.white,
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
        ListTile(
          leading: const Icon(Icons.perm_device_info),
          title: ValueListenableBuilder<Box>(
            valueListenable: deviceBox.listenable(keys: ['device_name']),
            builder: (context, box, widget) {
              return Text(box.get('device_name', defaultValue: 'explorer'));
            },
          ),
          subtitle: ValueListenableBuilder<Box>(
            valueListenable: deviceBox.listenable(keys: ['device_uuid']),
            builder: (context, box, widget) {
              final String uuid = box.get('device_uuid', defaultValue: 'mode');
              return InkWell(
                onTap: () => Clipboard.setData(ClipboardData(text: uuid)),
                child: Text(uuid),
              );
            },
          ),
          trailing: IconButton(
            icon: const Icon(Icons.edit),
            onPressed: () => Get.offAndToNamed("/register_device"),
          ),
        ),
        const Divider(
          height: 0,
        ),
        ListTile(
          leading: const Icon(Icons.stream),
          title: const Text('Stream location'),
          subtitle: const Text('enable live streaming of your location'),
          trailing: Obx(
            () => Switch(
              value: controller.streamOn.value,
              onChanged: (v) => controller.turnStream(v),
            ),
          ),
        ),
        const Divider(
          height: 0,
        ),
        ListTile(
          onTap: () => s.logout(),
          leading: const Icon(Icons.logout),
          title: const Text("Logout"),
          subtitle: Container(),
        ),
      ],
    );
  }
}
