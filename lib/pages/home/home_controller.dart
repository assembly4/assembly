import 'package:assembly/pages/friends/friends_page.dart';
import 'package:assembly/pages/friends/users_binding.dart';
import 'package:assembly/pages/map/available_devices_binding.dart';
import 'package:assembly/pages/map/map_controller_binding.dart';
import 'package:assembly/pages/map/map_page.dart';
import 'package:assembly/pages/settings/settings_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_navigation/src/routes/default_route.dart';
import 'package:get/get_navigation/src/routes/transitions_type.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

class HomeController extends GetxController {
  static HomeController get to => Get.find();

  RxInt currentIndex = 1.obs;

  final List pages = [
    {
      'name': 'friends',
      'url': '/friends',
      'title': 'friends',
      'icon': Icons.people,
    },
    {
      'name': 'map',
      'url': '/map',
      'title': 'Map',
      'icon': Icons.location_on_sharp,
    },
    {
      'name': 'settings',
      'url': '/settings',
      'title': 'Settings',
      'icon': Icons.settings,
    },
  ];

  void changePage(int index) {
    if (currentIndex.value == index) {
      return;
    }
    currentIndex.value = index;
    Get.offAndToNamed(pages[index]['url'], id: 1);
  }

  Route? onGenerateRoute(RouteSettings settings) {
    if (settings.name == '/friends') {
      return GetPageRoute(
          settings: settings,
          page: () => const FriendsPage(),
          binding: UsersBinding(),
          transition: Transition.leftToRightWithFade);
    }

    if (settings.name == '/map') {
      return GetPageRoute(
        settings: settings,
        page: () => const MapPage(),
        bindings: [
          AvailableDevicesBinding(),
          MapControllerBinding(),
        ],
        transition: currentIndex.value != 0
            ? Transition.leftToRightWithFade
            : Transition.rightToLeftWithFade,
      );
    }

    if (settings.name == '/settings') {
      return GetPageRoute(
        settings: settings,
        page: () => const SettingsPage(),
        transition: Transition.rightToLeftWithFade,
      );
    }

    return null;
  }
}
