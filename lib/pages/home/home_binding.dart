import 'package:assembly/pages/home/home_controller.dart';
import 'package:assembly/pages/home/worker_controller.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:get/get_instance/src/extension_instance.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(HomeController());
    Get.put(LocStreamWorkerController());
  }
}
