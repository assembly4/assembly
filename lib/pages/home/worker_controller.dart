import 'dart:async';
import 'package:assembly/components/services/services.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_rx/src/rx_workers/rx_workers.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:hive_flutter/hive_flutter.dart';

class LocStreamWorkerController extends GetxController {
  final box = Hive.box('box');

  RxBool streamOn = false.obs;
  late Worker streamWorker;
  Timer streamTimer = Timer(const Duration(), () {});

  @override
  void onInit() {
    streamOn.value = box.get('streamOn', defaultValue: false);
    super.onInit();
    streamWorker = ever(
        streamOn, (_) => streamOn.isTrue ? setupStream() : shutdownStream());
  }

  @override
  void onClose() {
    shutdownStream();
    streamWorker.dispose();
    super.onClose();
  }

  void setupStream() {
    s.logger.i('starting stream');
    s.zugClient.setupStreamDeviceLoc();
    streamTimer = Timer.periodic(
      const Duration(milliseconds: 1000),
      (_) => s.zugClient.sendLocToStreamDeviceLoc(),
    );
  }

  //turns on or off the stream by worker
  void turnStream(bool on) {
    streamOn.value = on;
    box.put('streamOn', on);
  }

  void shutdownStream() {
    s.logger.i('ending stream');
    streamTimer.cancel();
    s.zugClient.endLocationStream();
  }
}
