import 'package:assembly/constants.dart';
import 'package:assembly/pages/home/home_controller.dart';
import 'package:assembly/themes/color_palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter_snake_navigationbar/flutter_snake_navigationbar.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

class HomePage extends GetView<HomeController> {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: Navigator(
        key: Get.nestedKey(1),
        initialRoute: controller.pages[controller.currentIndex.value]['url'],
        onGenerateRoute: controller.onGenerateRoute,
      ),
      bottomNavigationBar: Obx(
        () => SnakeNavigationBar.color(
          elevation: 8,
          height: bottomBarHeight,
          behaviour: SnakeBarBehaviour.pinned,
          snakeShape: SnakeShape.rectangle,
          shape: const BeveledRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25),
              topRight: Radius.circular(25),
            ),
          ),
          snakeViewColor: color3,
          selectedItemColor: color1,
          unselectedItemColor: color3,
          backgroundColor: color1,
          showUnselectedLabels: true,
          showSelectedLabels: false,
          currentIndex: controller.currentIndex.value,
          onTap: (i) => controller.changePage(i),
          items: [
            for (var page in controller.pages)
              BottomNavigationBarItem(
                icon: Icon(page['icon']),
                label: page['title'],
              ),
          ],
        ),
      ),
    );
  }
}
