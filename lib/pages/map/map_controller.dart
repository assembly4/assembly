import 'package:assembly/components/services/services.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:grpc/grpc.dart';
import 'package:hive_flutter/hive_flutter.dart';

class MapController extends GetxController {
  final devicesBox = Hive.box('friendlyDevicesBox');

  late GoogleMapController mapController;

  RxSet<Marker> markersSet = <Marker>{}.obs;

  @override
  void onInit() {
    super.onInit();
    s.zugClient.setupDevicesStream();
    deviceSubscriptionStream();
  }

  @override
  void onClose() {
    mapController.dispose();
    s.zugClient.endDevicesLocStream();
    super.onClose();
  }

  Future<void> deviceSubscriptionStream() async {
    s.zugClient.subscription.listen(
          (data) {
        s.logger.d(data);
        markersSet.add(
          Marker(
            markerId: MarkerId(data.deviceId),
            position: LatLng(data.lat, data.long),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          ),
        );
      },
      onError: (err) {
        s.logger.e(err, 'error on stream receiver');
      },
      cancelOnError: false,
    );
  }
}
