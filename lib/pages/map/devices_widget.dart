import 'package:assembly/themes/color_palette.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

class DevicesWidget extends StatelessWidget {
  DevicesWidget({Key? key, required this.controller, this.reverse}) : super(key: key);

  final devicesBox = Hive.box('friendlyDevicesBox');
  final ScrollController controller;
  final bool? reverse;

  @override
  Widget build(BuildContext context) {
    bool current = false;
    if (reverse == null) {
      current = false;
    } else {
      current = reverse!;
    }

    return Container(
      color: color1.withOpacity(0.9),
      child: ValueListenableBuilder<Box>(
        valueListenable: devicesBox.listenable(),
        builder: (context, box, widget) {
          return ListView(
            controller: controller,
            reverse: current,
            padding: const EdgeInsets.all(0),
            children: [
              for (var owner in box.keys)
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    ListTile(
                      tileColor: color2,
                      textColor: Colors.white,
                      title: Text(owner),
                    ),
                    ValueListenableBuilder<Box>(
                      valueListenable: devicesBox.listenable(keys: [owner]),
                      builder: (context, box, widget) {
                        Map devices = box.get(owner);
                        return Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            for (var device in devices.entries)
                              ListTile(
                                textColor: Colors.white,
                                title: Text(device.value),
                                dense: true,
                                onTap: () => print('tap'),
                                hoverColor: color3,
                                subtitle: Text(
                                  device.key,
                                  style: const TextStyle(color: Colors.grey),
                                ),
                              ),
                          ],
                        );
                      },
                    ),
                  ],
                ),
            ],
          );
        },
      ),
    );
  }
}
