import 'package:assembly/constants.dart';
import 'package:assembly/pages/map/devices_widget.dart';
import 'package:assembly/pages/map/map_widget.dart';
import 'package:assembly/themes/color_palette.dart';
import 'package:flutter/material.dart';
import 'package:snapping_sheet/snapping_sheet.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapView extends StatelessWidget {
  MapView({Key? key}) : super(key: key);

  final ScrollController _sc = ScrollController();

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    if (width > 900) {
      return Stack(
        children: [
          MapWidget(),
          Positioned(
            left: 25,
            bottom: 60,
            width: 300,
            height: 400,
            child: DevicesWidget(
              controller: _sc,
            ),
          ),
        ],
      );
    }
    return SnappingSheet(
      snappingPositions: const [
        SnappingPosition.pixels(positionPixels: bottomBarHeight + 10),
        SnappingPosition.factor(positionFactor: 0.33),
        SnappingPosition.factor(positionFactor: 0.66),
        SnappingPosition.factor(positionFactor: 0.95),
      ],
      lockOverflowDrag: true,
      grabbingHeight: 25,
      grabbing: Material(
        color: color1.withOpacity(0.93),
        elevation: 8,
        shape: const BeveledRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25),
            topRight: Radius.circular(25),
          ),
        ),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 180,
                margin: const EdgeInsets.symmetric(vertical: 8),
                height: 5,
                color: color3,
              ),
              const Divider(
                height: 2,
                thickness: 2,
                color: color3,
              ),
            ]),
      ),
      sheetBelow: SnappingSheetContent(
        childScrollController: _sc,
        child: DevicesWidget(
          controller: _sc,
        ),
      ),
      child: MapWidget(),
    );
  }
}
