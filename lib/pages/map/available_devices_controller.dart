import 'dart:async';
import 'package:assembly/components/services/models.dart';
import 'package:assembly/components/services/services.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:hive_flutter/hive_flutter.dart';

class AvailableDevicesController extends GetxController {
  final devicesBox = Hive.box('friendlyDevicesBox');

  late Timer t1;

  @override
  void onInit() {
    queryAvailableDevices();
    super.onInit();
    t1 = Timer.periodic(
        const Duration(seconds: 5), (_) => queryAvailableDevices());
  }

  @override
  void onClose() {
    t1.cancel();
    super.onClose();
  }

  Future<void> queryAvailableDevices() async {
    try {
      DevicesModel data = await s.geneva.getDevices();
      for (DeviceModel device in data.devices) {
        // check for the device in the box so it won't update without a reason
        if (devicesBox.containsKey(device.owner)) {
          Map currentDevices = devicesBox.get(device.owner, defaultValue: {});
          if (currentDevices.containsKey(device.uuid)) {
            if (currentDevices[device.uuid] == device.name) {
              continue;
            }
          }
          currentDevices[device.uuid] = device.name;
          devicesBox.put(device.owner, currentDevices);
        } else {
          devicesBox.put(device.owner, {device.uuid: device.name});
        }
      }
      // check if there are any leftovers in box and recover from bad state
      num localDevicesAmount = 0;
      for (var x in devicesBox.values) {
        localDevicesAmount += x.length;
      }
      if (localDevicesAmount != data.devices.length) {
        print("differences ${devicesBox.keys.length} ${data.devices.length}");
        await devicesBox.clear();
        queryAvailableDevices();
      }
    } catch (err) {
      s.logger.e('error', err);
    }
  }
}


