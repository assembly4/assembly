import 'package:flutter/material.dart';
import 'map_view.dart';

class MapPage extends StatelessWidget {
  const MapPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MapView(),
    );
  }
}
