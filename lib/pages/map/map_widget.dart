import 'package:assembly/components/services/lucerne_client.dart';
import 'package:assembly/components/services/services.dart';
import 'package:assembly/pages/map/map_controller.dart';
import 'package:assembly_proto/lucerne/lucerne.pb.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

Future<void> sendCurrentLoc() async {
  s.logger.i("sending locs");
  List<Loc> locs = [];
  for (int i = 0; i < 180; i++) {
    locs.add(Loc(
      lat: i.toDouble(),
      long: -i.toDouble(),
      time: DateTime.now().toUtc().toIso8601String(),
    ));
  }

  LucerneClient().insertLocations(locs);
}

Future<void> selectLocation() async {
  s.logger.i("selecting location");
  LucerneClient().selectLocations(DateTime.utc(2021, 11).toIso8601String());
}

class MapWidget extends GetView<MapController> {
  const MapWidget({Key? key}) : super(key: key);

  static const _initialCameraPosition = CameraPosition(
    target: LatLng(37.7773, -122.431),
    zoom: 11.5,
  );

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => GoogleMap(
        padding: EdgeInsets.only(bottom: 65),
        initialCameraPosition: _initialCameraPosition,
        onMapCreated: (ctr) => controller.mapController = ctr,
        markers: controller.markersSet.value,
      ),
    );
    /*Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
          onPressed: () => sendCurrentLoc(),
          child: const Text("send location"),
        ),
        ElevatedButton(
          onPressed: () => selectLocation(),
          child: const Text("select location"),
        ),
        SfDateRangePicker(
          selectionMode: DateRangePickerSelectionMode.range,
          showActionButtons: true,
          maxDate: DateTime.now(),
          todayHighlightColor: Colors.red,
        ),
      ],
    );*/
  }
}
