import 'package:assembly/pages/login/authview_controller.dart';
import 'package:assembly/themes/color_palette.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class AuthBox extends StatelessWidget {
  AuthBox({Key? key}) : super(key: key);

  final AuthViewController c = Get.find();

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: 0.8,
      child: Obx(
        () => AnimatedContainer(
          duration: const Duration(milliseconds: 350),
          width: 300,
          height: 269,
          color: c.login.value ? color5 : color3,
          child: Padding(
            padding: const EdgeInsets.only(
                top: 10, bottom: 15, left: 15, right: 15),
            child: Flex(
              direction: Axis.vertical,
              children: [
                Expanded(
                  child: AutofillGroup(
                    child: Form(
                      key: c.authFormKey,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 81,
                            child: TextFormField(
                              cursorColor: c.login.value ? color3 : color5,
                              style: TextStyle(
                                color: c.login.value ? color3 : color5,
                              ),
                              autofillHints: const [AutofillHints.email],
                              controller: c.emailController,
                              decoration: InputDecoration(
                                icon: Icon(
                                  Icons.mail,
                                  color: c.login.value ? color3 : color5,
                                ),
                                focusColor: c.login.value ? color3 : color5,
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: c.login.value ? color3 : color5,
                                  ),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: c.login.value ? color3 : color5,
                                  ),
                                ),
                                labelStyle: TextStyle(
                                  color: c.login.value ? color3 : color5,
                                ),
                                labelText: 'Email *',
                              ),
                              keyboardType: TextInputType.emailAddress,
                              validator: c.emailValidator,
                            ),
                          ),
                          SizedBox(
                            height: 81,
                            child: TextFormField(
                              cursorColor: c.login.value ? color3 : color5,
                              style: TextStyle(
                                color: c.login.value ? color3 : color5,
                              ),
                              autofillHints: const [AutofillHints.password],
                              controller: c.passwordController,
                              decoration: InputDecoration(
                                icon: Icon(
                                  Icons.fingerprint,
                                  color: c.login.value ? color3 : color5,
                                ),
                                focusColor: c.login.value ? color3 : color5,
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: c.login.value ? color3 : color5,
                                  ),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: c.login.value ? color3 : color5,
                                  ),
                                ),
                                labelStyle: TextStyle(
                                  color: c.login.value ? color3 : color5,
                                ),
                                labelText: 'Password *',
                              ),
                              obscureText: true,
                              keyboardType: TextInputType.visiblePassword,
                              validator: c.passwordValidator,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: RoundedLoadingButton(
                          height: 40,
                          width: 110,
                          color: c.login.value ? color3 : color5,
                          valueColor: c.login.value ? color5 : color3,
                          controller: c.btnController,
                          onPressed: () => c.primaryButtonFunction(),
                          child: Text(
                            c.login.value ? "Login" : "Register",
                            style: GoogleFonts.montserrat(
                              color: c.login.value ? color5 : color3,
                            ),
                          ),
                        ),
                      ),
                      MouseRegion(
                        cursor: SystemMouseCursors.click,
                        child: GestureDetector(
                          onTap: () => c.secondaryButtonFunction(),
                          child: Text(
                            '${c.login.value ? "Register" : "Login"}?',
                            style: GoogleFonts.montserrat(
                              color: c.login.value ? color3 : color5,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
