import 'package:assembly/pages/login/auth_box.dart';
import 'package:assembly/pages/login/login_text.dart';
import 'package:assembly/themes/color_palette.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: color2,
      body: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: constraints.maxHeight,
                minWidth: constraints.maxWidth,
              ),
              child: Wrap(
                alignment: WrapAlignment.spaceEvenly,
                runAlignment: WrapAlignment.spaceEvenly,
                crossAxisAlignment: WrapCrossAlignment.center,
                verticalDirection: VerticalDirection.up,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: LoginText(),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: AuthBox(),
                  ),
                ],
              ),
            ),
          );
        },
      ),
      extendBodyBehindAppBar: true,
    );
  }
}
