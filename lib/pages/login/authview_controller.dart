import 'dart:async';
import 'package:assembly/components/auth/auth_controller.dart';
import 'package:assembly/components/services/models.dart';
import 'package:assembly/components/services/services.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/state_manager.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:hive_flutter/hive_flutter.dart';

class AuthViewController extends GetxController {
  static AuthViewController get to => Get.find();

  final passwordValidator = MultiValidator([
    RequiredValidator(errorText: 'Password required'),
  ]);
  final emailValidator = MultiValidator([
    RequiredValidator(errorText: 'Email must be specified'),
    EmailValidator(errorText: 'Invalid email'),
  ]);

  RxBool login = true.obs;

  final GlobalKey<FormState> authFormKey = GlobalKey<FormState>();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final RoundedLoadingButtonController btnController =
      RoundedLoadingButtonController();

  primaryButtonFunction() async {
    bool? valid = authFormKey.currentState?.validate();
    if (!valid!) {
      btnController.reset();
    } else {
      login.value ? loginFunction() : registerFunction();
    }
  }

  swap() async {
    if (login.value) {
      login.value = false;
    } else {
      login.value = true;
    }
  }

  secondaryButtonFunction() async {
    btnController.reset();
    var _email = emailController.value;
    var _password = passwordController.value;
    authFormKey.currentState?.reset();
    emailController.value = _email;
    passwordController.value = _password;
    swap();
  }

  loginFunction() async {
    Map model = LoginModel(
      email: emailController.text,
      password: passwordController.text,
    ).toJson();
    try {
      TokenModel token = await s.geneva.login(model);
      await s.saveToken(token.token);
      await s.populateClaims();
      AuthController.to.hasSession.value = true;
    } catch (err) {
      btnController.error();
      Timer(const Duration(milliseconds: 700), () => btnController.reset());
      Fluttertoast.showToast(
        msg: err.toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        webPosition: "center",
        backgroundColor: Colors.redAccent,
        textColor: Colors.white,
        fontSize: 16.0,
        timeInSecForIosWeb: 3,
      );
    }
  }

  registerFunction() async {
    Map model = LoginModel(
      email: emailController.text,
      password: passwordController.text,
    ).toJson();
    try {
      TokenModel token = await s.geneva.register(model);
      await s.saveToken(token.token);
      await s.populateClaims();
      AuthController.to.hasSession.value = true;
      Fluttertoast.showToast(
        msg: 'user ${Hive.box("claimsBox").get("Email")} created',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        webPosition: "center",
        backgroundColor: Colors.greenAccent,
        textColor: Colors.white,
        fontSize: 16.0,
        timeInSecForIosWeb: 3,
      );
    } catch (err) {
      btnController.error();
      Timer(const Duration(seconds: 2), () => btnController.reset());
      Fluttertoast.showToast(
        msg: err.toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        webPosition: "center",
        backgroundColor: Colors.redAccent,
        textColor: Colors.white,
        fontSize: 16.0,
        timeInSecForIosWeb: 3,
      );
    }
  }
}
