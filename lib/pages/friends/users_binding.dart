import 'package:assembly/pages/friends/users_controller.dart';
import 'package:get/get.dart';

class UsersBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(UsersController());
  }
}
