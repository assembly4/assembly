import 'package:assembly/pages/friends/search_bar.dart';
import 'package:assembly/pages/friends/users_controller.dart';
import 'package:assembly/themes/color_palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:implicitly_animated_reorderable_list/implicitly_animated_reorderable_list.dart';

class FriendsView extends StatelessWidget {
  FriendsView({Key? key}) : super(key: key);

  final acceptedFriendsBox = Hive.box('acceptedFriendsBox');
  final receivedFriendsBox = Hive.box('receivedFriendsBox');
  final sentFriendsBox = Hive.box('sentFriendsBox');
  final declinedFriendsBox = Hive.box('declinedFriendsBox');

  @override
  Widget build(BuildContext context) {
    return CustomSearchBar(
      child: ListView(
        children: [
          Container(
            height: 62,
          ),
          const ListTile(
            leading: Icon(
              Icons.people,
              color: Colors.white,
            ),
            title: Text(
              "Accepted",
              style: TextStyle(color: Colors.white),
            ),
            tileColor: color2,
          ),
          ValueListenableBuilder<Box>(
            valueListenable: acceptedFriendsBox.listenable(),
            builder: (context, box, widget) {
              List users = box.keys.toList();
              return FriendsWidget(
                  status: 'accepted', users: users.cast<String>());
            },
          ),
          const Padding(padding: EdgeInsets.all(0.5)),
          const ListTile(
            leading: Icon(
              Icons.call_received,
              color: Colors.white,
            ),
            title: Text(
              "Received",
              style: TextStyle(color: Colors.white),
            ),
            tileColor: color2,
          ),
          ValueListenableBuilder<Box>(
            valueListenable: receivedFriendsBox.listenable(),
            builder: (context, box, widget) {
              List users = box.keys.toList();
              return FriendsWidget(
                  status: 'received', users: users.cast<String>());
            },
          ),
          const Padding(padding: EdgeInsets.all(0.5)),
          const ListTile(
            leading: Icon(
              Icons.call_made,
              color: Colors.white,
            ),
            title: Text(
              "Sent",
              style: TextStyle(color: Colors.white),
            ),
            tileColor: color2,
          ),
          ValueListenableBuilder<Box>(
            valueListenable: sentFriendsBox.listenable(),
            builder: (context, box, widget) {
              List users = box.keys.toList();
              return FriendsWidget(
                status: 'sent',
                users: users.cast<String>(),
              );
            },
          ),
          const Padding(padding: EdgeInsets.all(0.5)),
          const ListTile(
            leading: Icon(
              Icons.cancel_outlined,
              color: Colors.white,
            ),
            title: Text(
              "Declined",
              style: TextStyle(color: Colors.white),
            ),
            tileColor: color2,
          ),
          ValueListenableBuilder<Box>(
            valueListenable: declinedFriendsBox.listenable(),
            builder: (context, box, widget) {
              List users = box.keys.toList();
              return FriendsWidget(
                status: 'declined',
                users: users.cast<String>(),
              );
            },
          ),
        ],
      ),
    );
  }
}

class FriendsWidget extends GetView<UsersController> {
  const FriendsWidget({Key? key, required this.status, required this.users})
      : super(key: key);

  final List<String> users;
  final String status;

  Widget buildTrailing(String item) {
    List<Widget> trailingItems = [];
    if (status == 'received') {
      trailingItems = [
        IconButton(
          onPressed: () => controller.declineFriendship(item),
          icon: const Icon(Icons.close),
        ),
        IconButton(
          onPressed: () => controller.acceptFriendship(item),
          icon: const Icon(Icons.done),
        ),
      ];
    }
    trailingItems.add(const Icon(Icons.menu));

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: trailingItems,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ImplicitlyAnimatedList<String>(
      shrinkWrap: true,
      items: users,
      areItemsTheSame: (oldItem, newItem) => oldItem == newItem,
      insertDuration: const Duration(milliseconds: 350),
      updateDuration: const Duration(milliseconds: 350),
      removeDuration: const Duration(milliseconds: 300),
      itemBuilder: (context, animation, item, index) {
        return SlideTransition(
          position: Tween<Offset>(
            begin: const Offset(1.0, 0.0),
            end: const Offset(0.0, 0.0),
          ).animate(animation),
          child: Slidable(
            endActionPane: ActionPane(
              extentRatio: 0.2,
              motion: const ScrollMotion(),
              children: [
                SlidableAction(
                  onPressed: (context) => showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        contentPadding: EdgeInsets.zero,
                        actionsPadding: EdgeInsets.zero,
                        shape: const BeveledRectangleBorder(),
                        content: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                "Delete friendship with $item?",
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: color4,
                                      fixedSize: const Size.fromHeight(48),
                                      shape: const BeveledRectangleBorder(),
                                    ),
                                    onPressed: () {
                                      Navigator.of(context).pop(true);
                                      controller.deleteFriendship(status, item);
                                    },
                                    child: const Text("Confirm"),
                                  ),
                                ),
                                const Padding(padding: EdgeInsets.all(1)),
                                Expanded(
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: color4,
                                      fixedSize: const Size.fromHeight(48),
                                      shape: const BeveledRectangleBorder(),
                                    ),
                                    onPressed: () =>
                                        Navigator.of(context).pop(false),
                                    child: const Text("Cancel"),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                  backgroundColor: Colors.redAccent,
                  foregroundColor: Colors.white,
                  icon: Icons.delete,
                  label: 'Delete',
                ),
              ],
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundColor: color4,
                foregroundColor: Colors.white,
                child: Text(item.substring(0, 1)),
              ),
              title: Text(item),
              trailing: buildTrailing(item),
            ),
          ),
        );
      },
      removeItemBuilder: (context, animation, oldItem) {
        return SlideTransition(
          position: Tween<Offset>(
            begin: const Offset(0.0, 0.0),
            end: const Offset(-1.0, 0.0),
          ).animate(animation),
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: color4,
              foregroundColor: Colors.white,
              child: Text(oldItem.substring(0, 1)),
            ),
            title: Text(oldItem),
          ),
        );
      },
    );
  }
}
