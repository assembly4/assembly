import 'package:assembly/components/services/models.dart';
import 'package:assembly/pages/friends/users_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:implicitly_animated_reorderable_list/implicitly_animated_reorderable_list.dart';
import 'package:implicitly_animated_reorderable_list/transitions.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';
import 'package:hive_flutter/hive_flutter.dart';

class CustomSearchBar extends GetView<UsersController> {
  CustomSearchBar({Key? key, required this.child}) : super(key: key);

  final Widget child;
  final acceptedFriendsBox = Hive.box('acceptedFriendsBox');
  final receivedFriendsBox = Hive.box('receivedFriendsBox');
  final sentFriendsBox = Hive.box('sentFriendsBox');
  final declinedFriendsBox = Hive.box('declinedFriendsBox');

  @override
  Widget build(BuildContext context) {
    return FloatingSearchBar(
      hint: 'Find users...',
      scrollPadding: const EdgeInsets.only(top: 16, bottom: 56),
      transitionDuration: const Duration(milliseconds: 800),
      transitionCurve: Curves.easeInOut,
      physics: const BouncingScrollPhysics(),
      autocorrect: false,
      width: 500,
      debounceDelay: const Duration(milliseconds: 300),
      onQueryChanged: (query) async {
        controller.query(query.toLowerCase());
      },
      onFocusChanged: (_) async {
        controller.query("");
      },
      leadingActions: [FloatingSearchBarAction.back()],
      actions: [
        FloatingSearchBarAction(
          showIfOpened: false,
          child: CircularButton(
            icon: const Icon(Icons.search),
            onPressed: () {},
          ),
        ),
      ],
      builder: (context, transition) {
        return Container(
          padding: const EdgeInsets.symmetric(vertical: 16),
          child: Material(
            color: Colors.white,
            elevation: 4.0,
            borderRadius: BorderRadius.circular(8),
            clipBehavior: Clip.antiAlias,
            child: controller.obx(
              (state) => ImplicitlyAnimatedList<UserModel>(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                items: state!.users,
                insertDuration: const Duration(milliseconds: 200),
                itemBuilder: (context, animation, item, i) {
                  return SizeFadeTransition(
                    animation: animation,
                    child: buildItem(context, item),
                  );
                },
                updateItemBuilder: (context, animation, item) {
                  return FadeTransition(
                    opacity: animation,
                    child: buildItem(context, item),
                  );
                },
                areItemsTheSame: (a, b) => a == b,
              ),
              onEmpty: buildItem(context, UserModel(email: 'no users found')),
            ),
          ),
        );
      },
      body: child,
    );
  }

  Widget buildItem(BuildContext context, UserModel user) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    Widget getIconButton(String email) {
      if (acceptedFriendsBox.containsKey(email)) {
        return const Icon(Icons.people);
      }
      if (receivedFriendsBox.containsKey(email)) {
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            IconButton(
              onPressed: () => controller.declineFriendship(email),
              icon: const Icon(Icons.close),
            ),
            IconButton(
              onPressed: () => controller.acceptFriendship(email),
              icon: const Icon(Icons.done),
            ),
          ],
        );
      }
      if (sentFriendsBox.containsKey(email)) {
        return const Icon(Icons.call_made);
      }
      if (declinedFriendsBox.containsKey(email)) {
        return const Icon(Icons.cancel_outlined);
      }
      return IconButton(
        onPressed: () => controller.requestFriendship(email),
        icon: const Icon(Icons.person_add),
      );
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ListTile(
          title: Text(
            user.email,
            style: textTheme.subtitle1,
          ),
          subtitle: const Text(
            "ni hao",
          ),
          trailing: getIconButton(user.email),
        ),
        const Divider(height: 0),
      ],
    );
  }
}
