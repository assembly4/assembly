import 'dart:async';
import 'package:assembly/components/services/models.dart';
import 'package:assembly/components/services/services.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_notifier.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:hive_flutter/hive_flutter.dart';

class UsersController extends GetxController with StateMixin<UsersModel> {
  final user = Hive.box('claimsBox').get("Email");

  final acceptedFriendsBox = Hive.box('acceptedFriendsBox');
  final receivedFriendsBox = Hive.box('receivedFriendsBox');
  final sentFriendsBox = Hive.box('sentFriendsBox');
  final declinedFriendsBox = Hive.box('declinedFriendsBox');

  late Timer t;

  @override
  void onInit() {
    getAllFriendShips();
    super.onInit();
    t = Timer.periodic(
        const Duration(seconds: 5), (_) => getAllFriendShips());
  }

  @override
  void onClose() {
    t.cancel();
    super.onClose();
  }

  Future<bool> getAllFriendShips() async {
    bool i = await s.friendsSvc.getAccepted();
    bool ii = await s.friendsSvc.getRequested();
    bool iii = await s.friendsSvc.getDeclined();
    if (i || ii || iii) {
      return true;
    }
    return false;
  }

  Future<void> query(String queryString) async {
    try {
      UsersModel data = await s.geneva.getUsers(queryString);
      data.users.isEmpty
          ? change(data, status: RxStatus.empty())
          : change(data, status: RxStatus.success());
    } catch (err) {
      change(null, status: RxStatus.error("error"));
    }
  }

  Future<void> requestFriendship(String addressee) async =>
      _putFriendship('request', addressee);

  Future<void> acceptFriendship(String addressee) async =>
      _putFriendship('accept', addressee);

  Future<void> declineFriendship(String addressee) async =>
      _putFriendship('decline', addressee);

  Future<void> _putFriendship(String status, String addressee) async {
    try {
      switch (status) {
        case 'request':
          {
            await s.geneva.requestFS(addressee);
            Fluttertoast.showToast(
              msg: "Request sent to $addressee",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              webPosition: "center",
              backgroundColor: Colors.lightGreen,
              textColor: Colors.white,
              fontSize: 16.0,
              timeInSecForIosWeb: 3,
            );
            getAllFriendShips().then((v) => v ? update() : null);
          }
          break;
        case 'accept':
          {
            await s.geneva.acceptFS(addressee);
            Fluttertoast.showToast(
              msg: "You are now friends with $addressee",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              webPosition: "center",
              backgroundColor: Colors.lightGreen,
              textColor: Colors.white,
              fontSize: 16.0,
              timeInSecForIosWeb: 3,
            );
            getAllFriendShips().then((v) => v ? update() : null);
          }
          break;
        default:
          {
            await s.geneva.declineFS(addressee);
            Fluttertoast.showToast(
              msg: "$addressee request declined",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              webPosition: "center",
              backgroundColor: Colors.lightGreen,
              textColor: Colors.white,
              fontSize: 16.0,
              timeInSecForIosWeb: 3,
            );
            getAllFriendShips().then((v) => v ? update() : null);
          }
          break;
      }
    } catch (err) {
      s.logger.e("error", err);
    }
  }

  Future<void> deleteFriendship(String status, String addressee) async {
    try {
      await s.geneva.deleteFS(addressee);
      switch (status) {
        case 'accepted':
          {
            getAllFriendShips().then((v) => v ? update() : null);
          }
          break;
        case 'received':
          {
            getAllFriendShips().then((v) => v ? update() : null);
          }
          break;
        case 'declined':
          {
            getAllFriendShips().then((v) => v ? update() : null);
          }
          break;
        default:
          {
            getAllFriendShips().then((v) => v ? update() : null);
          }
          break;
      }
    } catch (err) {
      s.logger.e('error', err);
    }
  }
}
