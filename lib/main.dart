import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:assembly/components/auth/auth_binding.dart';
import 'package:assembly/components/routes/routes.dart';
import 'package:assembly/components/services/services.dart';

void main() async {
  await s.init();
  runApp(const App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: "assembly",
      getPages: AppRoutes.routes,
      initialBinding: AuthBinding(),
      initialRoute: s.initialPath,
      defaultTransition: Transition.noTransition,
      debugShowCheckedModeBanner: false,
    );
  }
}
