import 'package:assembly/components/location/location_controller.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:get/get_instance/src/extension_instance.dart';

class LocationBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(LocationController(), permanent: true);
  }
}