import 'package:assembly/pages/device/devices_binding.dart';
import 'package:assembly/pages/device/register_device_page.dart';
import 'package:assembly/pages/home/home_binding.dart';
import 'package:assembly/pages/home/home_page.dart';
import 'package:assembly/pages/login/authview_binding.dart';
import 'package:assembly/pages/login/login_page.dart';
import 'package:get/get.dart';

class AppRoutes {
  static final routes = [
    GetPage(
      name: '/',
      page: () => const HomePage(),
      binding: HomeBinding(),
      transition: Transition.upToDown,
    ),
    GetPage(
      name: '/login',
      page: () => const LoginPage(),
      binding: AuthViewBinding(),
      transition: Transition.upToDown,
    ),
    GetPage(
      name: '/register_device',
      page: () => const RegisterDevicePage(),
      binding: DevicesBinding(),
      transition: Transition.upToDown,
    ),
  ];
}
