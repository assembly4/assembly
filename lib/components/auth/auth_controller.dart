import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_rx/src/rx_workers/rx_workers.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get/get_utils/src/platform/platform.dart';
import 'package:hive_flutter/hive_flutter.dart';

class AuthController extends GetxController {
  static AuthController get to => Get.find();
  final claimsBox = Hive.box('claimsBox');
  final deviceBox = Hive.box('deviceBox');

  RxBool hasSession = false.obs;

  @override
  void onInit() {
    ever(hasSession, (_) {
      if (hasSession.isTrue) {
        GetPlatform.isWeb
            ? Get.offAllNamed('/')
            : Get.offAllNamed('/register_device');
        Fluttertoast.showToast(
          msg: "Logged in as ${claimsBox.get('Email')}",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          webPosition: "center",
          backgroundColor: Colors.lightGreen,
          textColor: Colors.white,
          fontSize: 16.0,
          timeInSecForIosWeb: 3,
        );
      } else {
        Get.offAllNamed('/login');
        Fluttertoast.showToast(
          msg: "Logged out",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          webPosition: "center",
          backgroundColor: Colors.lightGreen,
          textColor: Colors.white,
          fontSize: 16.0,
          timeInSecForIosWeb: 3,
        );
      }
    });
    super.onInit();
  }
}
