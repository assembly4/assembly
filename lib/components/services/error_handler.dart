import 'package:assembly/components/services/services.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get_connect/http/src/response/response.dart';

dynamic errorHandler(Response response) {
  switch (response.statusCode) {
    case 200:
    case 201:
      Fluttertoast.showToast(
        msg: "${response.statusCode}",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        webPosition: "center",
        backgroundColor: Colors.redAccent,
        textColor: Colors.white,
        fontSize: 16.0,
        timeInSecForIosWeb: 1,
        webBgColor: '#${Colors.redAccent.value.toRadixString(16).substring(2)}',
      );
      break;
    case 500:
    default:
      Fluttertoast.showToast(
        msg: "${response.statusCode}",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        webPosition: "center",
        backgroundColor: Colors.redAccent,
        textColor: Colors.white,
        fontSize: 16.0,
        timeInSecForIosWeb: 1,
        webBgColor: '#${Colors.redAccent.value.toRadixString(16).substring(2)}',
      );
  }
  s.logger.e(response.statusCode, response.body);
}
