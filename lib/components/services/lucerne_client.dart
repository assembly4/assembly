import 'dart:async';
import 'package:assembly/components/services/services.dart';
import 'package:assembly/constants.dart';
import 'package:grpc/grpc.dart';
import 'package:assembly_proto/lucerne/lucerne.pbgrpc.dart';
import 'package:hive_flutter/hive_flutter.dart';

class LucerneClient {
  final _deviceBox = Hive.box('deviceBox');

  late LocationClient _stub;

  Future<void> insertLocations(List<Loc> locs) async {
    final token = await s.storage.read(key: "token");
    final deviceId = await _deviceBox.get("device_uuid");
    final locations = DeviceLocs(
      deviceId: deviceId,
      token: token,
      loc: locs,
    );
    final channel = ClientChannel(
      "lucerne.$domain",
      port: 443,
      options: const ChannelOptions(credentials: ChannelCredentials.secure()),
    );
    _stub = LocationClient(
      channel,
      options: CallOptions(
        timeout: const Duration(seconds: 10),
      ),
    );

    try {
      final response = await _stub.insertLoc(locations);
      s.logger.i(response.status);
    } catch (e) {
      s.logger.e(e);
    }
    await channel.shutdown();
  }

  Future<void> selectLocations(String start,
      {String end = "", String deviceId = ""}) async {
    final token = await s.storage.read(key: "token");
    final dId = deviceId == "" ? await _deviceBox.get("device_uuid") : deviceId;
    final endDate = end == "" ? DateTime.now().toUtc().toIso8601String() : end;
    final dtr = DeviceTimeRange(
      deviceId: dId,
      token: token,
      start: start,
      end: endDate,
    );
    final channel = ClientChannel(
      "lucerne.$domain",
      port: 443,
      options: const ChannelOptions(credentials: ChannelCredentials.secure()),
    );
    _stub = LocationClient(
      channel,
      options: CallOptions(
        timeout: const Duration(seconds: 10),
      ),
    );

    try {
      await for (var locsList in _stub.selectLoc(dtr)) {
        // as proto reflection treats zero values as not set message field
        // we check that and set it manually
        for (var loc in locsList.loc) {
          loc.hasLat() ? null : loc.lat = 0;
          loc.hasLong() ? null : loc.long = 0;
        }
      }
    } catch (e) {
      s.logger.e(e);
    }
    await channel.shutdown();
  }
}
