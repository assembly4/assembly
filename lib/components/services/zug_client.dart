import 'dart:async';
import 'package:assembly/components/services/services.dart';
import 'package:assembly/constants.dart';
import 'package:assembly_proto/zug/zug.pbgrpc.dart';
import 'package:grpc/grpc.dart';
import 'package:hive_flutter/hive_flutter.dart';

class ZugClient {
  final _deviceBox = Hive.box('deviceBox');
  final _friendlyDevicesBox = Hive.box('friendlyDevicesBox');
  final _box = Hive.box('box');

  // stream for sending current device location
  StreamController<LocationWithSetup> _streamLocC =
      StreamController<LocationWithSetup>();

  // stream for sending list of subscription devices
  StreamController<Devices> _devicesSubStreamC = StreamController<Devices>();

  // stream for receiving devices locations
  StreamController<Devices> _devicesLocStreamC = StreamController<Devices>();

  late ResponseStream subscription;
  late RedisClient _stub;
  late ClientChannel _channel;

  void init() {
    _channel = ClientChannel(
      "zug.$domain",
      port: 443,
      options: const ChannelOptions(credentials: ChannelCredentials.secure()),
    );
    _stub = RedisClient(_channel);
  }

  Future<void> setupStreamDeviceLoc() async {
    /// sends current device location to server
    s.logger.i('setting up _streamLocC');
    if (_streamLocC.isClosed) {
      s.logger.i('creating new _streamLocC');
      _streamLocC = StreamController<LocationWithSetup>();
    }

    // start stream and register retry/reconnect function if something fails
    _stub.streamDeviceLoc(_streamLocC.stream).whenComplete(() {
      bool shouldBeRunning = _box.get('streamOn', defaultValue: false);
      if (shouldBeRunning) {
        endLocationStream();
        setupStreamDeviceLoc();
      }
    });

    sendOptionsToStreamDeviceLoc();
  }

  Future<void> sendOptionsToStreamDeviceLoc() async {
    final token = await s.storage.read(key: "token");
    final deviceId = await _deviceBox.get("device_uuid");

    _streamLocC.sink.add(
      LocationWithSetup(
        options: Options(
          token: token,
          deviceId: deviceId,
        ),
      ),
    );
  }

  Future<void> sendLocToStreamDeviceLoc() async {
    final deviceId = await _deviceBox.get("device_uuid");

    if (_streamLocC.isClosed) {
      s.logger.w('stream closed, cannot add values to it');
      return;
    }
    // we subtract 0.5s from current time because otherwise
    // the server might fail on lte(lesserthanorequal) validation
    // due to time difference between server and client
    // TODO: remove timestamps entirely (not needed while streaming as it is real time)
    String now = DateTime.now()
        .toUtc()
        .subtract(const Duration(milliseconds: 1000))
        .toIso8601String();

    _streamLocC.sink.add(
      LocationWithSetup(
        location: Location(deviceId: deviceId, lat: 5, long: 10, time: now),
      ),
    );
  }

  void endLocationStream() {
    _streamLocC.close();
  }

  void setupDevicesStream() {
    /// sends list of devices to receive location streams from
    s.logger.i('setting up _devicesSubStreamC');
    if (_devicesSubStreamC.isClosed) {
      s.logger.i('creating new _devicesSubStreamC');
      _devicesSubStreamC = StreamController<Devices>();
    }

    // start stream and register retry/reconnect function if something fails
    subscription = _stub.subscribeToDevices(_devicesSubStreamC.stream);

    sendDevicesToDevicesStream();
  }

  Future<void> sendDevicesToDevicesStream() async {
    final token = await s.storage.read(key: "token");
    List<String> uuids = [];

    // add all friends' devices to uuids List
    for (var devices in _friendlyDevicesBox.values) {
      uuids.addAll(devices.keys.toList().cast<String>());
    }
    s.logger.i(uuids, 'sending device to subscription');
    _devicesSubStreamC.sink.add(Devices(token: token, uuids: uuids));
  }

  void endDevicesLocStream() {
    _devicesSubStreamC.close();
    _devicesLocStreamC.close();
  }
}
