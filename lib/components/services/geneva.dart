import 'package:assembly/components/services/error_handler.dart';
import 'package:assembly/components/services/models.dart';
import 'package:assembly/components/services/services.dart';
import 'package:assembly/constants.dart';
import 'package:get/get_connect/connect.dart';
import 'package:get/get_connect/http/src/request/request.dart';

class GenevaProvider extends GetConnect {
  @override
  bool get allowAutoSignedCert => domain == "localhost" ? true : false;

  @override
  void onInit() {
    httpClient.baseUrl = "https://geneva.$domain";
    httpClient.defaultContentType = "application/json";

    /// TODO: now = without token -> if fails then add one and retry
    /// TODO: how = token added based on parameter (endpoints with mandatory auth and these without)
    httpClient.addAuthenticator((Request request) async {
      String? token = await s.storage.read(key: "token");
      request.headers['Authorization'] = "Bearer $token";
      return request;
    });
    httpClient.maxAuthRetries = 3;
    super.onInit();
  }

  Future<bool> healthCheck() async {
    final res = await get("/api/v1/health");
    if (res.statusCode == 200) {
      return true;
    }
    errorHandler(res);
    return false;
  }

  Future<TokenModel> login(Map data) async {
    final res = await post("/api/v1/login", data);

    if (res.statusCode == 200) {
      return TokenModel.fromJson(res.body);
    }
    errorHandler(res);
    throw res.body["message"];
  }

  Future<TokenModel> register(Map data) async {
    final res = await post("/api/v1/register", data);

    if (res.statusCode == 201) {
      return TokenModel.fromJson(res.body);
    }
    errorHandler(res);
    throw res.body["message"];
  }

  Future<bool> verify(String token) async {
    Map data = TokenModel(token: token).toJson();
    final res = await post("/api/v1/verify", data);

    if (res.statusCode == 200) {
      return true;
    }
    errorHandler(res);
    return false;
  }

  Future<UsersModel> getUsers(String queryString) async {
    final res = await get("/api/v1/users?queryString=$queryString");

    if (res.statusCode == 200) {
      return UsersModel.fromJson(res.body);
    }
    errorHandler(res);
    throw res.body["message"];
  }

  Future<DevicesModel> getDevices([String? addressee]) async {
    final paramString = addressee == null ? "" : "?addressee=$addressee";
    final res = await get("/api/v1/devices$paramString");

    if (res.statusCode == 200) {
      return DevicesModel.fromJson(res.body);
    }
    errorHandler(res);
    throw res.body["message"];
  }

  Future<String> putDevice(String uuid, String name) async {
    final data = DeviceModel(uuid: uuid, name: name).toJson();
    final res = await put("/api/v1/devices", data);

    if (res.statusCode == 200 || res.statusCode == 201) {
      return res.body["message"].toString();
    }
    errorHandler(res);
    throw res.body["message"];
  }

  Future<FriendsModel> _getFS(String status) async {
    final res = await get("/api/v1/friendships?status=$status");

    if (res.statusCode == 200) {
      return FriendsModel.fromJson(res.body);
    }
    errorHandler(res);
    throw res.body["message"];
  }

  Future<FriendshipModel> _putFS(String addressee, String status) async {
    final data = FriendshipModel(addressee: addressee, status: status).toJson();
    final res = await put("/api/v1/friendships", data);

    if (res.statusCode == 200 || res.statusCode == 201) {
      return FriendshipModel.fromJson(res.body);
    }
    errorHandler(res);
    throw res.body["message"];
  }

  Future<FriendshipModel> deleteFS(String addressee) async {
    final res = await delete("/api/v1/friendships?addressee=$addressee");

    if (res.statusCode == 200) {
      return FriendshipModel.fromJson(res.body);
    }
    errorHandler(res);
    throw res.body["message"];
  }

  Future<FriendsModel> getAcceptedFS() async => _getFS('Accepted');

  Future<FriendsModel> getRequestedFS() async => _getFS('Requested');

  Future<FriendsModel> getDeclinedFS() async => _getFS('Declined');

  Future<FriendshipModel> acceptFS(String addressee) async =>
      _putFS(addressee, 'Accepted');

  Future<FriendshipModel> requestFS(String addressee) async =>
      _putFS(addressee, 'Requested');

  Future<FriendshipModel> declineFS(String addressee) async =>
      _putFS(addressee, 'Declined');
}
