import 'package:assembly/components/auth/auth_controller.dart';
import 'package:assembly/components/services/friends_services.dart';
import 'package:assembly/components/services/geneva.dart';
import 'package:assembly/components/services/lucerne_client.dart';
import 'package:assembly/components/services/zug_client.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_utils/src/platform/platform.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:logger/logger.dart';
import 'dart:async';

Services s = Services();

class Services {
  final Logger logger = Logger();
  final FlutterSecureStorage storage = const FlutterSecureStorage(
    iOptions: IOSOptions(accessibility: IOSAccessibility.first_unlock),
    aOptions: AndroidOptions(encryptedSharedPreferences: true),
  );
  final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

  late FriendsServices friendsSvc;
  late LucerneClient lucerneClient;
  late ZugClient zugClient;

  String initialPath = "/login";

  var geneva = Get.put(GenevaProvider());

  Future<void> init() async {
    await Hive.initFlutter();
    await initBoxes();

    //await assertPermissions();
    //await startBackgroundLocService();
    initialPath = await initialCheck();

    if (GetPlatform.isAndroid) {
      Hive.box('box')
          .put('hardwareName', (await deviceInfo.androidInfo).model.toString());
    }
    if (GetPlatform.isIOS) {
      Hive.box('box').put('hardwareName',
          (await deviceInfo.iosInfo).utsname.machine.toString());
    }
    // it is already in initialCheck()
    //if (GetPlatform.isWeb) {
    //  Hive.box('box').put(
    //      'hardwareName', (await deviceInfo.webBrowserInfo).browserName.name);
    //}

    friendsSvc = FriendsServices();
    lucerneClient = LucerneClient();
    zugClient = ZugClient();
    zugClient.init();
  }

  Future<void> initBoxes() async {
    await Hive.openBox('box', compactionStrategy: (entries, deletedEntries) {
      return deletedEntries > 50;
    });
    await Hive.openBox('claimsBox', compactionStrategy: (entries, deletedEntries) {
      return deletedEntries > 50;
    });
    await Hive.openBox('deviceBox', compactionStrategy: (entries, deletedEntries) {
      return deletedEntries > 50;
    });
    await Hive.openBox('friendlyDevicesBox',
        compactionStrategy: (entries, deletedEntries) {
      return deletedEntries > 50;
    });
    await Hive.openBox('acceptedFriendsBox',
        compactionStrategy: (entries, deletedEntries) {
      return deletedEntries > 50;
    });
    await Hive.openBox('receivedFriendsBox',
        compactionStrategy: (entries, deletedEntries) {
      return deletedEntries > 50;
    });
    await Hive.openBox('sentFriendsBox',
        compactionStrategy: (entries, deletedEntries) {
      return deletedEntries > 50;
    });
    await Hive.openBox('declinedFriendsBox',
        compactionStrategy: (entries, deletedEntries) {
      return deletedEntries > 50;
    });
  }

  Future<String> initialCheck() async {
    String? token = await storage.read(key: "token");
    if (token != null) {
      if (await geneva.verify(token)) {
        if (Hive.box('deviceBox').containsKey("device_uuid") ||
            GetPlatform.isWeb) {
          return "/";
        }
        return "/register_device";
      }
    }
    return '/login';
  }

  Future<void> saveToken(String token) async {
    await storage.write(key: "token", value: token);
  }

  Future<void> logout() async {
    storage.delete(key: "token");
    AuthController.to.hasSession.value = false;
    await clearBoxes();
  }

  Future<void> clearBoxes() async {
    await Hive.box('claimsBox').clear();
    await Hive.box('deviceBox').clear();
    await Hive.box('friendlyDevicesBox').clear();
    await Hive.box('acceptedFriendsBox').clear();
    await Hive.box('receivedFriendsBox').clear();
    await Hive.box('sentFriendsBox').clear();
    await Hive.box('declinedFriendsBox').clear();
  }

  Future<void> populateClaims() async {
    String? token = await storage.read(key: "token");
    JwtDecoder.decode(token!).forEach((key, value) {
      Hive.box('claimsBox').put(key, value);
    });
  }

  //Future<void> startBackgroundLocService() async {
  //  await bg.BackgroundGeolocation.ready(
  //    bg.Config(
  //        desiredAccuracy: bg.Config.DESIRED_ACCURACY_HIGH,
  //        distanceFilter: 10.0,
  //        stopOnTerminate: false,
  //        startOnBoot: true,
  //        debug: true,
  //        logLevel: bg.Config.LOG_LEVEL_VERBOSE),
  //  );
  //  (bg.State state) =>
  //      !state.enabled ? bg.BackgroundGeolocation.start() : null;
  //}

  //ture<void> assertPermissions() async {
  //bool serviceEnabled;
  //LocationPermission permission;

  //permission = await Geolocator.checkPermission();
  //print(permission);

  //// Test if location services are enabled.
  //serviceEnabled = await Geolocator.isLocationServiceEnabled();
  //if (!serviceEnabled) {
  //  Geolocator.requestPermission();
  //  // Location services are not enabled don't continue
  //  // accessing the position and request users of the
  //  // App to enable the location services.
  //  return Future.error('Location services are disabled.');
  //}
  //await location.enableBackgroundMode(enable: true);

  //_allowBackground = await location.isBackgroundModeEnabled();
  //if (!_allowBackground) {
  //  _serviceEnabled = await location.requestBack;
  //  if (!_serviceEnabled) {
  //    return;
  //  }
  //}

  //_serviceEnabled = await location.serviceEnabled();
  //if (!_serviceEnabled) {
  //  _serviceEnabled = await location.requestService();
  //  if (!_serviceEnabled) {
  //    return;
  //  }
  //}
//
  //_permissionGranted = await location.hasPermission();
  //if (_permissionGranted == PermissionStatus.denied) {
  //  _permissionGranted = await location.requestPermission();
  //  if (_permissionGranted != PermissionStatus.granted) {
  //    return;
  //  }
  //}
  //}
}
