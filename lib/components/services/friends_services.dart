import 'package:assembly/components/services/models.dart';
import 'package:assembly/components/services/services.dart';
import 'package:hive_flutter/hive_flutter.dart';

class FriendsServices {
  final claimsBox = Hive.box('claimsBox');
  final acceptedFriendsBox = Hive.box('acceptedFriendsBox');
  final receivedFriendsBox = Hive.box('receivedFriendsBox');
  final sentFriendsBox = Hive.box('sentFriendsBox');
  final declinedFriendsBox = Hive.box('declinedFriendsBox');

  /// all get functions return booleans to indicate whether a change was made
  /// in the underlying box
  Future<bool> getAccepted() async {
    final user = claimsBox.get("Email");
    Set accepted = {};

    try {
      FriendsModel data = await s.geneva.getAcceptedFS();
      for (FriendshipModel friendship in data.friends) {
        if (friendship.addressee == user) {
          accepted.add(friendship.requester);
        } else {
          accepted.add(friendship.addressee);
        }
      }
      return saveSet('accepted', accepted);
    } catch (err) {
      s.logger.e('error', err);
    }
    return false;
  }

  Future<bool> getRequested() async {
    final user = claimsBox.get("Email");
    Set received = {};
    Set sent = {};

    try {
      FriendsModel data = await s.geneva.getRequestedFS();
      for (FriendshipModel friendship in data.friends) {
        // if user is the addressee of friendship
        if (friendship.addressee == user) {
          received.add(friendship.requester);
        } else {
          sent.add(friendship.addressee);
        }
      }
      if (saveSet('received', received) || saveSet('sent', sent)) {
        return true;
      }
    } catch (err) {
      s.logger.e('error', err);
    }
    return false;
  }

  Future<bool> getDeclined() async {
    final user = claimsBox.get("Email");
    Set declined = {};

    try {
      FriendsModel data = await s.geneva.getDeclinedFS();
      for (FriendshipModel friendship in data.friends) {
        if (friendship.addressee == user) {
          declined.add(friendship.requester);
        } else {
          declined.add(friendship.addressee);
        }
      }
      return saveSet('declined', declined);
    } catch (err) {
      s.logger.e('error', err);
    }
    return false;
  }

  bool saveSet(String status, Set newSet) {
    Box box;
    switch (status) {
      case 'accepted':
        {
          box = acceptedFriendsBox;
        }
        break;
      case 'received':
        {
          box = receivedFriendsBox;
        }
        break;
      case 'declined':
        {
          box = declinedFriendsBox;
        }
        break;
      default:
        {
          box = sentFriendsBox;
        }
        break;
    }
    Set currentSet = box.keys.toSet();
    Set toAdd = newSet.difference(currentSet);
    Set toDelete = currentSet.difference(newSet);
    box.putAll(Map.fromIterable(toAdd, key: (item) => item));
    box.deleteAll(toDelete);
    if (toAdd.isEmpty && toDelete.isEmpty) {
      return false;
    }
    return true;
  }
}
