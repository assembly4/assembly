import 'package:flutter/foundation.dart';

class TokenModel {
  TokenModel({
    required this.token,
  });

  String token;

  factory TokenModel.fromJson(Map<String, dynamic> json) => TokenModel(
        token: json["access_token"],
      );

  Map toJson() => {"token": token};
}

class LoginModel {
  LoginModel({
    required this.email,
    required this.password,
  });

  String email;
  String password;

  Map toJson() => {"email": email, "password": password};
}

class UserModel {
  UserModel({
    required this.email,
  });

  String email;
}

class UsersModel {
  UsersModel({
    required this.users,
  });

  List<UserModel> users;

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UsersModel && listEquals(other.users, users);
  }

  @override
  int get hashCode => users.hashCode;

  factory UsersModel.fromJson(Map<String, dynamic> json) => UsersModel(
        users: json["users"] != null
            ? json["users"]
                .map<UserModel>((value) => UserModel(email: value))
                .toList()
            : [],
      );
}

class DeviceModel {
  DeviceModel({
    required this.uuid,
    required this.name,
    this.owner,
  });

  String uuid;
  String name;
  String? owner;

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is DeviceModel && other.hashCode == hashCode;
  }

  @override
  int get hashCode => uuid.hashCode;

  Map toJson() => {"name": name, "uuid": uuid};
}

class DevicesModel {
  DevicesModel({
    this.addressee,
    required this.devices,
  });

  String? addressee = "";
  List<DeviceModel> devices;

  factory DevicesModel.fromJson(Map<String, dynamic> json) => DevicesModel(
        devices: json["devices"] != null
            ? json["devices"]
                .map<DeviceModel>((value) => DeviceModel(
                      uuid: value["uuid"],
                      owner: value["owner"],
                      name: value["name"],
                    ))
                .toList()
            : [],
      );
}

class FriendsModel {
  FriendsModel({
    required this.friends,
  });

  List<FriendshipModel> friends = [];

  factory FriendsModel.fromJson(Map<String, dynamic> json) => FriendsModel(
        friends: json["users"] != null
            ? json["users"]
                .map<FriendshipModel>((value) => FriendshipModel(
                      status: value["status"],
                      requester: value["requester"],
                      addressee: value["addressee"],
                    ))
                .toList()
            : [],
      );
}

class FriendshipModel {
  FriendshipModel({
    this.status,
    required this.addressee,
    this.requester,
    this.message,
  });

  String? status = "Requested";
  String? requester = "";
  String addressee = "";
  String? message = "";

  factory FriendshipModel.fromJson(Map<String, dynamic> json) =>
      FriendshipModel(
        message: json["message"],
        addressee: '',
      );

  Map toJson() => {"addressee": addressee, "status": status};
}
