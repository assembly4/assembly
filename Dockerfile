#FROM cirrusci/flutter:2.8.1 AS build

#COPY . /app
#WORKDIR /app
#RUN flutter build web


FROM nginx:1.21.5

#COPY --from=build /server/bin /server

COPY build/web /var/www/html/app
COPY assembly.conf /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/nginx.conf
